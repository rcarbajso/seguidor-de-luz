#include <p18F4520.h>
#include <delays.h>
#include<stdlib.h>
#include<adc.h>
#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF

void main(){
    int Canal;
	OpenADC(ADC_FOSC_16 & ADC_6_TAD & ADC_RIGHT_JUST, 
	ADC_REF_VDD_VSS & ADC_INT_OFF, ADC_2ANA);
	TRISD=0x00;
	LATD=0x00;
TRISA=0xFF;
	while(1){
		SetChanADC(2);
        Delay10TCYx(5);
		ConvertADC();
		while(BusyADC()==1){}
		Canal=ReadADC();
	    LATD=Canal>>2;
		Delay1KTCYx(100);
	}
}