#include <p18F4520.h>
#include <delays.h>
#include <adc.h>

#include "./tipos.h"

#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF


#define USE_C18_routines


#define select_ADC(ch) { ADCON0 &= 0b11000011;  ADCON0 += (ch<<2); }


void main(void) 
  {
   uint16 res;

       
   TRISC=0; PORTC=0; TRISB=0; PORTB=0; 
   TRISAbits.TRISA2=1;
 //  TRISA=0xFF;

   OpenADC(ADC_FOSC_16 & ADC_RIGHT_JUST & ADC_6_TAD, 
           ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS, 7);
 
    while(1)
     {
      #ifdef USE_C18_routines
          SetChanADC(ADC_CH2);   
          Delay10TCYx(5);
          ConvertADC();
          while( BusyADC() );
          res = ReadADC(); 
      #else
         select_ADC(2);
         Delay10TCYx(5);
         ADCON0bits.GO=1;
         while(ADCON0bits.GO);
         res = ADRESH; res=(res<<8)+ ADRESL;
      #endif

      PORTB=res>>2;
      Delay10TCYx(5);
     }

}
