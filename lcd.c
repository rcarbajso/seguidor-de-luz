#include <p18F4520.h>
#include <delays.h>
#include <xlcd.h>
#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF

unsigned char Unidad;
const rom unsigned char Display1[10]={0x00,0xFF,0x00,0xF0,0x00,0xFF,0x00,0xF0,0x00,0xFF};
void DelayFor18TCY(void){
	Delay10TCYx(2);
}
void DelayPORXLCD(void){
	Delay1KTCYx(15);
}
void DelayXLCD(void){
	Delay1KTCYx(2);
}
void comandXLCD(unsigned char a){
	BusyXLCD();
	WriteCmdXLCD(a);
}
void gotoxyXLCD(unsigned char x,unsigned char y){
	unsigned char direccion;
	if(y!=1)
		direccion=0x40;
	else
		direccion=0;
	direccion+=x-1;
	comandXLCD(0x80 | direccion);
}
void main(void) 
{
  OpenXLCD(FOUR_BIT & LINES_5X7);
  comandXLCD(0x06);
  comandXLCD(0x0C);
while(1)
{
  comandXLCD(0x01);
  putrsXLCD("Hola mundo");

  }

}
