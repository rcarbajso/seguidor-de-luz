#include <p18F4520.h>
#include <delays.h>
#include <xlcd.h>
#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF
void main(void) 
{

 ADCON1=0x0F;  // No PORTS used as AD
 TRISD=0x00; 
 TRISB=0x00; 
 LATB=0x00; 
 Unidad=0;
 while(1)
  {
	  LATD=Display1[Unidad];
	  LATB=0xFF;
	  Delay1KTCYx(1000);
	  LATB=0x00;
	  ++Unidad;
	  if(Unidad==9){
		  Unidad=0;
	  }
  
  }
}