#include <p18f4520.h> 
#include <delays.h>  
#pragma config OSC = HS 
#pragma config MCLRE = ON 
#pragma config WDT = OFF
#define set_TMR0(x) {TMR0H=(x>>8); TMR0L=(x&0x00FF);}
#define led PORTAbits.RA0 
void high_isr(void); 
#pragma code high_vector=0x08 
void interrupt_at_high_vector(void) 
{ _asm GOTO high_isr _endasm 
} 
#pragma code
#pragma interrupt high_isr // asi se define un metodo isr de alta prioridad
int TMR0_ini, pulse=400;
void  high_isr (void) 
{ 
unsigned char i;
if(INTCONbits.TMR0IF==1) 
{ 
// inicio zona critica
T0CONbits.TMR0ON=0;
INTCONbits.GIE=0;
PORTCbits.RC0=~PORTCbits.RC0;
   if (PORTCbits.RC0) TMR0_ini= 25-pulse; //65536-(pulse-25);
     else TMR0_ini= 45536+pulse+25;    //65536-(20000-(pulse-25))
   set_TMR0(TMR0_ini);
   INTCONbits.TMR0IF=0;
  
 /* se pone el flag a 0, esto es lo que hace saltar la interrupcion y 
						salta cuando TMROH y TMROL son 255 + 1 (overflow)*/
T0CONbits.TMR0ON=1;
INTCONbits.GIE=1;
// salida zona critica

 
} 
} void main(void) 
{ 
TRISC=0;
ADCON1=0x0f; 
TRISA=0b00000110;
PORTCbits.RC0 = 0; 
// init
INTCONbits.GIE=1; // Habilita interrupciones
INTCONbits.TMR0IE=1; 

T0CONbits.T08BIT=0; 
T0CONbits.T0CS=0;
T0CONbits.PSA=0;
T0CONbits.T0PS2=0;
T0CONbits.T0PS1=0; 
T0CONbits.T0PS0=1;
// fin init
//-------------------------- 
led=0; 
while(1) 
{ 
}
}