#include <p18F4520.h>
#include <delays.h>
#include<adc.h>
#include<stdlib.h>  
#pragma config OSC=HS
#pragma config PWRT = OFF, BOREN = OFF 
#pragma config WDT = OFF, WDTPS = 128
#pragma config PBADEN = OFF, LVP = OFF

//MACROS Y ETIQUETAS
#define set_TMR0(x) {TMR0H=(x>>8); TMR0L=(x&0x00FF);} 
#define ENAB_INTS INTCONbits.GIE=1;
#define DISAB_INTS INTCONbits.GIE=0;
#define ENAB_TIM T0CONbits.TMR0ON=1;
#define DISAB_TIM T0CONbits.TMR0ON=0;
#define TMR0_flag  INTCONbits.TMR0IF
#define LCD_CLEAR 1
#define LCD_HOME 2
#define LCD_FIRST_ROW 128
#define LCD_SECOND_ROW 192

#define LCD_CURSOR_OFF 12
#define LCD_UNDERLINE 14
#define LCD_BLINK_ON 15

#define LCD_CURSOR_LEFT 16
#define LCD_CURSOR_RIGHT 20
#define LCD_TURN_OFF 0
#define LCD_TURN_ON 8

#define LCD_SHIFT_RIGHT 28
#define LCD_SHIFT_LEFT  24


#define LCD_ENABLE PORTBbits.RB5
#define LCD_RS PORTBbits.RB4

#define LCD_STROBE  LCD_ENABLE=1; Delay10TCYx(1); LCD_ENABLE=0; 

//VARIABLES
unsigned short TMR0_value=60535;
int placa_izqda,placa_dcha,error,aux;
int ancho=2000;
char txt[16]="Lectura placa";
unsigned char k,pos;
void high_isr(void); 
#pragma code high_vector=0x08 
void interrupt_at_high_vector(void) 
{ _asm GOTO high_isr _endasm 
} 
#pragma code
#pragma interrupt high_isr // asi se define un metodo isr de alta prioridad

void  high_isr (void) { 
	if(TMR0_flag) { 
		PORTDbits.RD0=~PORTDbits.RD0;
		TMR0_value=(PORTDbits.RD0==0)?25535:65535-ancho;
		set_TMR0(TMR0_value);
		TMR0_flag=0;
	} 
} 



//-------------------------------------
void lcd_send_nibble(short int nibble)
{
 PORTB = (PORTB & 0xF0) + (nibble);
 LCD_STROBE;
}


//----------------------------------------
// Send a byte to the LCD.
void lcd_command(short int cmd)
{
 LCD_RS=0; //Register = Instruction);
 LCD_ENABLE=0; //output_low(LCD_E);
 Delay10TCYx(30);  //delay_us(60);
 lcd_send_nibble(cmd >> 4);
 lcd_send_nibble(cmd & 0x0F);
 Delay10KTCYx(1);  //delay_ms(2);
}


void lcd_init(void)
{
short int i;

TRISB=0b00000000;

LCD_RS=0;
LCD_ENABLE=0;

Delay10KTCYx(8);  //delay_ms(15);
lcd_send_nibble(0x03); Delay10KTCYx(3);  //delay_ms(5);
lcd_send_nibble(0x03); Delay10TCYx(85);  //delay_us(170);
lcd_send_nibble(0x03); Delay10TCYx(85);  //delay_us(170); 
lcd_send_nibble(0x02);  // 4 bit mode


// set interface length
lcd_command(0x28); Delay10KTCYx(3); //delay_ms(5);
// Enable display/Cursor
lcd_command(0x0C); Delay10KTCYx(3); //delay_ms(5);
// Clear Display
lcd_command(0x01); Delay10KTCYx(3); //delay_ms(5);
// Set Cursor Move Direction
lcd_command(0x06); Delay10KTCYx(3); //delay_ms(5);
}


void move_cursor(short int row, short int col)
{
short int address;

 address= (row)? 0x40:0x00;   //row=0; if (row) address = 0x40;
 address+=col;
 lcd_command( 0x80 | address);
}


//-----------------------------
void lcd_putc(char c)
 {
 LCD_ENABLE=0; LCD_RS=1; // Data
 lcd_send_nibble(c >> 4);
 lcd_send_nibble(c & 0xf);
 Delay10TCYx(80); //delay_us(160);
 }


void lcd_text(char *txt)
{
 char k;
 char null=' ';
 k=0; while(txt[k]) lcd_putc(txt[k++]);
 //for(k=0;k<n;k++) lcd_putc(txt[k]);

 lcd_putc(null);
}
//MAIN
void main(void) 
{ 
ADCON1=0x0F;
TRISD=0;
PORTDbits.RD0=0;
//Configuración TIMER
T0CONbits.T0CS=0;
T0CONbits.T08BIT=0;//MODO 16 BIT
T0CONbits.PSA=1;//SIN PRESCALER
ENAB_INTS;
INTCONbits.TMR0IE=1;
ENAB_TIM;   
OpenADC(ADC_FOSC_16  & ADC_RIGHT_JUST & ADC_6_TAD, 
ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS, 7);
TRISC=0; PORTC=0; TRISB=0; PORTB=0;
TRISAbits.TRISA2=1;
lcd_init();
  //move_cursor(0,0); 
Delay10KTCYx(5);
lcd_command(LCD_CLEAR);  
Delay10KTCYx(5); 
lcd_command(LCD_FIRST_ROW);
move_cursor(0,0);  // Fila 0, Col 0
lcd_text(txt); // Vuelca cadena texto
move_cursor(1,0);  // Fila 1, col 0

	OpenADC(ADC_FOSC_16  & ADC_RIGHT_JUST & ADC_6_TAD, 
           ADC_INT_OFF & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS, 7);
	TRISC=0; PORTC=0; TRISB=0; PORTB=0;TRISD=0;PORTD=0;
TRISAbits.TRISA2=1;
	lcd_init();
  //move_cursor(0,0); 
  Delay10KTCYx(5);
  lcd_command(LCD_CLEAR);  
  Delay10KTCYx(5); 
  lcd_command(LCD_FIRST_ROW);
  move_cursor(0,0);  // Fila 0, Col 0
  lcd_text(txt); // Vuelca cadena texto
  move_cursor(1,0);  // Fila 1, col 0
  
	while(1){
		move_cursor(1,0);  // Fila 1, col 0;

		//Placa izqda==RA2
		SetChanADC(ADC_CH2);
        Delay10TCYx(5);
		ConvertADC();
		while(BusyADC()){}
		placa_izqda=ReadADC();
		//Placa dcha==RA1
		SetChanADC(ADC_CH1);
        Delay10TCYx(5);
		ConvertADC();
		while(BusyADC()){}
		placa_dcha=ReadADC();
	    itoa(placa_izqda,txt);
		lcd_text(txt);
		move_cursor(1,5);
	    itoa(placa_dcha,txt);
		lcd_text(txt);
		//error=placa_izqda-placa_dcha
		move_cursor(1,10);
		error=placa_dcha*(1.2)-placa_izqda;
		//IZQDA <=> +
		//DCHA <=> -
		if (error < 25 && error > -25){
			ancho = 3000;
		}
		else {
		if (error > 25){
		ancho=3000 + (error - 25) *8;
			if(ancho>5000){
			ancho=5000;
			}
			else if(ancho<1000){
			ancho=1000;
			}
		}
		else if (error < -25){
			ancho=3000 + (error + 25) *8;
			if(ancho>5000){
			ancho=5000;
			}
			else if(ancho<1000){
			ancho=1000;
			}
		}
		}

		itoa(ancho,txt);
		lcd_text(txt);
}
}